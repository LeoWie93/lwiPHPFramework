<?php
/**
 * Created by PhpStorm.
 * User: leowie
 * Date: 23.05.18
 * Time: 19:56
 */

class Pages extends Controller
{

    public function __construct()
    {
    }

    public function index()
    {
        $data =
            [
                'title' => 'lwiPHPFramework',
            ];
        $this->render('pages/index', $data);
    }

    public function about()
    {
        $data =
            [
                'title' => 'About us',
            ];
        $this->render('pages/about', $data);
    }
}