<?php
// DB Params
define('DB_HOST', 'localhost');
define('DB_USER', '<USER>');
define('DB_PASS', '<PASS>');
define('DB_NAME', '<DB>');

// App Root
define('APPROOT', dirname(__DIR__));
// URL Root
define('URLROOT', '<URL>');
// Site Name
define('SITENAME', '<SITENAME>');